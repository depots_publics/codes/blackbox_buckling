# -*- coding: utf-8 -*-
"""
Blackbox related to the buckling of a two-bar truss

This buckling blackbox is used for one of the numerical tests of the following publication (currently under revisions):
[1] Audet, C., Batailly, A., Kojtych S. (2021) “Escaping unknown discontinuous regions in blackbox optimization”,
SIAM Journal on Optimization

#### Prerequisites
The code has been tested with the following versions of Python and its packages:
- Python 3.8
- scipy version 1.8.2
- numpy version 1.19.2

### Reference for the equations
Non-linear Finite Element Analysis of Solids and Structures, Crisfield 2012, pp 4-5

### Input
The input should be a text file, given in argument and containing one single line with values x1 x2 such as:
x1 = A0 scaled = the cross section area of the bar
x2 = Fext scaled = the magnitude of the imposed force at the top of the truss (>0 if directed downward)
Input should be provided scaled between 0 and 100 included.

The real bounds on Fext and A0 can be modified in the options. The default is :
50 mm2 <= A0 <= 300 mm2
5 000 N <= Fext <= 10 000 N

### Output
The output is the deflection of the linking point between the two bars and is positive downward.

The output is printed according to nomad specifications and the exit status of the black box is 0 for a
a successful computation and 1 when an error occurs (for instance if the Newton solver had not reach convergence).

Solène Kojtych, 04.05.2021
"""

import numpy as np
from scipy import optimize
import sys
import csv

# ------ Optimization problem parameters
max_disp = 0.2              # maximum displacement allowed (above which solution is unfeasible)

# Bounds for the scaling
Fmin = 5000                 # (Newton) corresponds to the scaled force 0
Fmax = 10000                # (Newton) corresponds to the scaled force 100
A0min = 50 * 10 ** -6       # (m2) corresponds to the scaled cross section 0
A0max = 300 * 10 ** -6      # (m2) corresponds to the scaled cross section 0

# Parameters of the Newton method used to solve the nonlinear equation in the blackbox
# Termination criteria
tolerance_stop = 10 ** -5  # tolerance
n_max = 10000              # max number of iterations (intentionally high so that the method is always stopped
                           # by the tolerance criteria

# ----- Mechanical system parameters
# Young's modulus
E = 70000 * 10 ** 6        # (MPa)
# Initial height h
h = 0.15                   # (meters)
# Initial width b
b = 1                      # (meters)
# Length l0 of the bar in the original configuration
l0 = np.sqrt(h ** 2 + b ** 2)  # (meters)


# ----- Auxiliary functions

def compute_Fint(v, A):
    """
    Computation of the nonlinear internal force in the bar of cross section A under imposed displacement v
    :param v: vertical displacement (meters)
    :param A: cross section (square meters)
    :return: Internal force (Newton)
    """
    # Length of the bar in the current configuration
    bar_length = np.sqrt(l0 ** 2 - 2 * h * v + v ** 2)

    # Internal force
    F_int = -A * E * ((bar_length - l0) / l0) * ((h - v) / l0)

    return F_int


def g(v, A0, Fext):
    """
    # Function for resolution of the system equilibrium  g(v)=Fext-Fint(v,AO) = 0
    :param v: vertical displacement (m)
    :param A0: cross section Area (mm2)
    :param Fext: vertical load (N)
    :return: Fext - Fint(v,A0)
    """
    # Mechanical equilibrium
    result = Fext - compute_Fint(v, A0)
    return result


def g_prime(v, A0, Fext):
    """
    # Function g'(v) for the resolution of g(v)=0 with Newton
    :param v: vertical displacement (m)
    :param A0: cross section Area (mm2)
    :param Fext: vertical load (N)
    :return: g'(v)
    """
    # Length of the bar in the current configuration
    l_bar = np.sqrt(l0 ** 2 - 2 * h * v + v ** 2)

    # Derivative of Fint with respect to v (corresponds to g'(v))
    result = -(A0 * E / l0) * (((h - v) / l_bar) ** 2 + (l_bar - l0) / l0)
    return result


def f_second(v, A0, Fext):
    """
    # Function g''(v) for the resolution of g(v)=0 with Newton
    :param v: vertical displacement (m)
    :param A0: cross section Area (mm2)
    :param Fext: vertical load (N)
    :return: g'(v)
    """
    # Length of the bar in the current configuration
    bar_length = np.sqrt(l0 ** 2 - 2 * h * v + v ** 2)

    # Second derivative
    result = -(A0 * E / l0 ** 2) * (v - h) * (1 / l0 + 1 / bar_length)
    return result


# ---- Main code for the buckling blackbox

def buckling_bb(input_file):
    """
    Main function of the blackbox : read x in the input file, unscale x components into cross-section A and external load
    Fext, compute the resulting displacement v and print the objective function and the constraint.
    Usable as blackbox with NOMAD (https://www.gerad.ca/nomad/).
    :param input_file: text file with one line containing values A F scaled between 0 and 100
    :return: 0 exit code if the computation was successful, 1 otherwise
    """
    # --------------------- INITIALIZATION ------------------------------------
    ### Read variables file (A, F)
    with open(input_file, 'r') as file:
        my_reader = csv.reader(file, delimiter=' ')
        x = next(my_reader)

    ### Unscaling of the variables
    A_scaled = float(x[0])  # cross-section area
    F_scaled = float(x[1])  # imposed force

    # Check lower and upper bounds
    if not (0 <= A_scaled <= 100) or not (0 <= F_scaled <= 100):
        sys.exit(1)  # error code for Nomad

    # Real values
    A0 = A0min + A_scaled * (A0max - A0min) / 100
    Fext = Fmin + F_scaled * (Fmax - Fmin) / 100

    # --------------------- COMPUTATION OF THE DISPLACEMENT v ---------------
    ### Solving of the system
    # Localization of the first max of the curve x=displacement,y=force
    # (in order to set an appropriate initial condition)
    displacement_values = np.linspace(0, 0.35, 10000)
    force_values = compute_Fint(displacement_values, A0)
    difference = np.diff(force_values)
    force_first_max = force_values[np.where(difference < 0)[0][0]]

    # Choice of initial displacement v0 (meters) to catch the appropriate branch of solution with Newton
    if Fext < 0.99 * force_first_max:
        # If Fext does not cause buckling
        v0 = 0.000001
    else:
        # If Fext causes buckling
        v0 = displacement_values[-1] * 1.5

    # Computation of the solution
    displacement, r = optimize.newton(g, v0,
                                      fprime=g_prime, fprime2=f_second,
                                      full_output=True, disp=False,
                                      args=(A0, Fext),
                                      tol=tolerance_stop, maxiter=n_max)

    # --------------------- BLACKBOX OUTPUT FOR NOMAD ------------------------
    # Objective function
    fobj = A_scaled

    # Constraint
    if r.converged:
        # print("A0 = {} mm2, Fext = {} N, convergence :{}, displacement {}"
        # .format(A0, Fext, r.converged, displacement))
        constraint = displacement - max_disp
    else:
        constraint = np.nan

    # Print of the output
    if np.isnan(constraint) or np.isnan(fobj):
        # Returns an error status for nomad if Newton hadn't converged
        sys.exit(1)
    else:
        print(fobj, end=' ')  # objective function
        print(constraint, end='\n')  # constraint
        sys.exit(0)  # blackbox success evaluation status


# ---- Run of the blackbox
if __name__ == "__main__":
    # Read the name of variable file as argument
    arguments = sys.argv
    input_file_name = str(arguments[1])

    # Run
    buckling_bb(input_file_name)

