# Blackbox related to the buckling of a two-bar truss

## About
This repository contains the source code of the buckling blackbox used for one of the numerical tests of the following
publication (currently under revisions):

[1] Audet, C., Batailly, A., Kojtych S. (2021) “Escaping unknown discontinuous regions in blackbox optimization”,
SIAM Journal on Optimization

This source code should be cited as follows:

[2] Kojtych, S. (2021) “Blackbox related to the buckling of a two-bar truss [source code]”, available at 
https://gitlab.lava.polymtl.ca/depots_publics/codes/blackbox_buckling

## Content of the blackbox
The blackbox computes the vertical displacement v(x) undergone by a two-bar truss under a specified load. Details on 
this computation may be found in [1] and in the following reference:

[3] Crisfield, M.A. Non-linear finite element analysis of solids and structures, vol. 1, Wiley New-York, 1993, pp. 4-5

The input of the blackbox is a vector x whose coordinates are x1, the cross-section area of the two bars, and x2, the 
magnitude of the load applied on the truss. The input should be provided between 0 and 100 included and are unscaled 
in the blackbox (see the source code for more details on the bounds). 

The output of the blackbox is a print of the objective function (equal to x1) and of the constraint c(x)=v(x)-vmax
 where vmax is a bound on the displacement. 

The exit status of the blackbox is 0 for a successful evaluation and 1 if an error occurs.

The parameters related to the two-bar truss mechanics and the optimization problem may be changed in the beginning of 
the source code.

## Use
To test the blackbox standalone, run :

```python3 buckling_blackbox.py x.txt```

The expected output is :
```
50.0 0.1252281041935992
```
When executed standalone, the input of the blackbox should be provided through a text file similar to ```x.txt```
containing on one line the values x1 and x2 separated by a space and scaled between 0 and 100.

The blackbox is fully compatible with the NOMAD [4] framework (https://www.gerad.ca/nomad/) and has been tested with the 
version 3.9.1 of NOMAD.

[4] C. Audet, S. Le Digabel, and C. Tribes. NOMAD user guide. Technical Report G-2009-37, Les cahiers du GERAD, 2009.

## Prerequisites
The code is implemented in Python 3.8 and has been tested with the following versions Python and packages:
- Python 3.8
- Scipy: version 1.8.2
- Numpy: version 1.19.2


## License
This source code is copyright © 2021 Solène Kojtych under license GPLv3